import os
import sys
import hashlib
import filecmp

def main():
	argv=sys.argv
	length=len(argv)
	if(length<2):
		print "Check the Syntax"
		exit()
	if(length==3):
		filename=os.path.basename(argv[1])
		pathOfFile=os.path.abspath(argv[1])
		m = hashlib.md5()
		m.update(pathOfFile)
		dirname=m.hexdigest()
		if not os.path.isdir('/home/bawaji94/ShabbirBawaji/l3cube-sponser/SVC/ForFile/svc/'+dirname):
			print "The file does not have any commits"
			exit()
		file=open(argv[1],"w+")
		versionfile=open("./svc/"+dirname+"/"+''.join(filename.split('.')[:-1])+argv[2]+"."+filename.split('.')[-1],'r')
		mainfile=open("./svc/"+dirname+"/"+filename,"w+")
		content=versionfile.read()
		file.write(content)
		mainfile.write("v"+argv[2]+"\n"+content)
		print "version"		
	else:
		filename=os.path.basename(argv[1])
		if not os.path.isfile(argv[1]):
			print "file does not exist"
			exit()
		pathOfFile=os.path.abspath(argv[1])
		m = hashlib.md5()
		m.update(pathOfFile)
		dirname=m.hexdigest()
		print dirname
		with open(argv[1]) as f:
			content = f.readlines()
		# print content
		if len(content)>20:
			print "Cannot commit no of lines exceded"
			exit()
		i=0
		out=""
		for s in content:
			i=i+1
			if len(s)>10:
				print "Cannot commit"
				print "Line number "+str(i)+" has exceeded the limit of 10 characters per line" 
				exit()
			out=out+s
		if not os.path.isdir('/home/bawaji94/ShabbirBawaji/l3cube-sponser/SVC/ForFile/svc/'+dirname):
			os.makedirs('/home/bawaji94/ShabbirBawaji/l3cube-sponser/SVC/ForFile/svc/'+dirname)
			newfile=open('/home/bawaji94/ShabbirBawaji/l3cube-sponser/SVC/ForFile/svc/'+dirname+'/'+filename,'w+')
			newfile.write("v0\n"+pathOfFile+'\n'+out)
			f=filename.split('.')[:-1]
			extension=filename.split('.')[-1]
			newfilev=open('/home/bawaji94/ShabbirBawaji/l3cube-sponser/SVC/ForFile/svc/'+dirname+'/'+''.join(f)+'0.'+extension,'w+')
			newfilev.write(out)
		else:
			v=len(os.listdir('./svc/'+dirname))
			i=0
			f=filename.split('.')[:-1]
			extension=filename.split('.')[-1]
			while(i<v-1):
				if filecmp.cmp(pathOfFile,'./svc/'+dirname+'/'+''.join(f)+str(i)+'.'+extension):
					print "Version "+str(i)+" contains the contents"
					exit()
				i=i+1
			newfile=open('/home/bawaji94/ShabbirBawaji/l3cube-sponser/SVC/ForFile/svc/'+dirname+'/'+filename,'w+')
			newfile.write("v"+str(v-1)+"\n"+pathOfFile+'\n'+out)
			newfilev=open('/home/bawaji94/ShabbirBawaji/l3cube-sponser/SVC/ForFile/svc/'+dirname+'/'+''.join(f)+str(v-1)+'.'+extension,'w+')
			newfilev.write(out)
		print "commit"
main()