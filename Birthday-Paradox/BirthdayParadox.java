import java.io.*;
import java.util.*;
public class BirthdayParadox
{
	String dates[];
	String shuffledDates[];
	int repeat;
	BirthdayParadox(int n)
	{
		repeat=n;
		int d=repeat;
		dates=new String[d*366];
		shuffledDates=new String[d*366];
		int c=0;
		for(int i=1;i<=12;i++)
		{
			int noOfDays=30;
			if(i==1 || i==3 || i==5 || i==7 || i==8 || i==10 || i==12)
			{
				noOfDays=31;
			}
			else if(i==4 || i==6 || i==9 || i==11)
			{
				noOfDays=30;
			}
			else if(i==2)
			{
				noOfDays=29;
			}
			for(int j=1;j<=noOfDays;j++)
			{
				for(int k=c;k<c+d;k++)
				{
					dates[k]=j+"/"+i;
				}
				c+=d;
			}
		}
		int len=d*366;
		for(int i=0;i<d*366;i++)
		{
			int index=(int)(Math.random()*(len-1-i));
			shuffledDates[i]=dates[index];
			if(index!=len-1-i)
			{
				dates[index]=dates[len-1-i];
			}
		}
	}
	public void Experiment(int noPeople,int noTrial)
	{
		int no_of_pairs=noPeople*(noPeople-1)/2;
		double for1person=365.0/366;
		double probablity_of_not_matching=Math.pow(for1person,no_of_pairs);
		double probOfHappening=1-probablity_of_not_matching;
		System.out.println("Probablity of matching birdays occuring is "+(probOfHappening*100)+"%");
		int prob=0;
		for(int i=0;i<noTrial;i++)
		{
			System.out.println();
			if(doExperiment(noPeople))
			{
				prob++;
			}
		}
		System.out.println("In "+noTrial+" trials "+prob+" had atleast one matching pair");
		System.out.println("That is "+(prob*100.0/noTrial)+"% probablity on the experiment conducted");
		System.out.println("Probablity of matching birdays occuring is "+(probOfHappening*100)+"%");
	}
	public boolean doExperiment(int noPeople)
	{
		int d=repeat;
		boolean flag=false;
		String birthdate[]=new String[noPeople];
		String shuffledDates1[]=new String[d*366];
		System.out.println("The birthdates of all the people in randome are :");
		int len=d*366;
		for(int i=0;i<len;i++)
		{
			shuffledDates1[i]=shuffledDates[i];
		}
		for(int i=0;i<noPeople;i++)
		{
			int index=(int)(Math.random()*(len-1-i));
			birthdate[i]=shuffledDates1[index];
			if(index!=len-1-i)
			{
				shuffledDates1[index]=shuffledDates1[len-1-i];
			}
			for(int j=0;j<i;j++)
			{
				if(birthdate[i].equals(birthdate[j]))
				{
					flag=true;
				}
			}
			System.out.print(birthdate[i]+" ");
		}
		System.out.println();
		if(flag)
		{
			System.out.println("Matching");
		}
		else
		{
			System.out.println("Not Matching");
		}
		return flag;
	}
	public static void main(String args[])
	{
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the number of people in the room");
		int n=scan.nextInt();
		System.out.println("Enter the number of trials you want to conduct");
		int no=scan.nextInt();
		System.out.println("Enter the number of time a date is repeated");
		int r=scan.nextInt();
		BirthdayParadox b=new BirthdayParadox(r);
		b.Experiment(n,no);
	}
}